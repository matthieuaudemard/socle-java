package taridev.socle.presentation;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TableModelCreator {

    private  TableModelCreator() {
        // Constructeur privé pour empécher la création TableModelCreator
    }

    public static <T> TableModel createTableModel(Class<T> beanClass, List<T> list) throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(beanClass);
        List<String> columns = new ArrayList<>();
        List<Method> getters = new ArrayList<>();

        for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
            String name = descriptor.getName();
            if (name.equals("class")) {
                continue;
            }
            name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
            String[] s = name.split("(?=\\p{Upper})");
            StringBuilder displayName = new StringBuilder();

            for (String s1 : s) {
                displayName.append(s1).append(" ");
            }

            columns.add(displayName.toString());
            Method m = descriptor.getReadMethod();
            getters.add(m);
        }

        return new AbstractTableModel() {
            @Override
            public String getColumnName(int column) {
                return columns.get(column);
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return columns.size();
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                try {
                    return getters.get(columnIndex).invoke(list.get(rowIndex));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}