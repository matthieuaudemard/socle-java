package taridev.socle.presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class HeaderPanel extends JPanel {

    private transient BufferedImage image;

    public HeaderPanel() {
        setBackground(Color.WHITE);
        setLayout(new GridBagLayout());
        try {
            image = ImageIO.read(new File("ressources/images/headerlocation.png"));
            setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, getWidth()- image.getWidth(), 0, this); // see javadoc for more info on the parameters
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension minSize = super.getMinimumSize();
        minSize.height = image.getHeight();
        return minSize;
    }

    @Override
    public Dimension getMaximumSize() {
        Dimension maxSize = super.getMaximumSize();
        maxSize.height = image.getHeight();
        return maxSize;
    }
}
