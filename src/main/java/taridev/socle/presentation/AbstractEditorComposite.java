package taridev.socle.presentation;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

public abstract class AbstractEditorComposite<T extends BaseVo> extends JPanel {

    private T vo;
    private JPanel mainComposite;
    private JPanel optionComposite;

    public AbstractEditorComposite(T vo) {
        this.vo = vo;

        optionComposite = new JPanel();
        mainComposite = new JPanel();
        optionComposite = new JPanel();

        setLayout(new MigLayout("insets 20 20 0 20", "[grow]", "[][]"));
        add(mainComposite, "dock center");
        add(optionComposite, "dock south");

        innerCreateContent();
        innerCreateListener();
    }

    public T getVo() {
        return vo;
    }

    public void setVo(T vo) {
        this.vo = vo;
    }

    protected abstract void innerCreateContent();

    protected abstract void innerCreateListener();

    public JPanel getMainComposite() {
        return mainComposite;
    }

    public void setMainComposite(JPanel mainComposite) {
        this.mainComposite = mainComposite;
    }

    public JPanel getOptionComposite() {
        return optionComposite;
    }

    public void setOptionComposite(JPanel optionComposite) {
        this.optionComposite = optionComposite;
    }
}
