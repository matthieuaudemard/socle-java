package taridev.socle.presentation;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BaseResultComposite<T extends BaseVo> extends JPanel {

    private JTable table = new JTable();;
    private transient List<T> vo = new ArrayList<>();
    private JPanel optionsPanel = new JPanel();

    BaseResultComposite() {
        super(new BorderLayout(), true);
        add(new JScrollPane(table), BorderLayout.CENTER);
        add(optionsPanel, BorderLayout.EAST);
        add(new JPanel(), BorderLayout.WEST);
    }

    public void setModel(TableModel tableModel) {
        table.setModel(tableModel);
    }

    public List<T> getVo() {
        return vo;
    }

    public void setVo(List<T> vo) {
        this.vo = vo;
    }

    public JPanel getOptionPanel() {
        return optionsPanel;
    }

    public T getSelectRow() {
        return table.getSelectedRow() < 0 ? null : vo.get(table.getSelectedRow());
    }
}
