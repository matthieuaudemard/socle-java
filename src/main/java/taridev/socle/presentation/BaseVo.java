package taridev.socle.presentation;

import java.beans.PropertyChangeSupport;

public class BaseVo {

    protected PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public BaseVo() {
        // Constructeur d'entité vide.
    }
}
