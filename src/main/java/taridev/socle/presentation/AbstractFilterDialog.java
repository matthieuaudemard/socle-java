package taridev.socle.presentation;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractFilterDialog<E extends BaseVo, T extends BaseVo> extends AbstractDialog {

    protected JPanel filterComposite = new JPanel();
    private JPanel filterCompositeContainer = new JPanel(new BorderLayout());
    protected BaseResultComposite<T> resultComposite;

    public AbstractFilterDialog() {
        this(null);
    }

    public AbstractFilterDialog(E filterVo) {
        super();
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        mainComposite.setLayout(new BorderLayout());
        resultComposite = new BaseResultComposite<>();
        filterCompositeContainer.add(filterComposite, BorderLayout.PAGE_START);
        filterCompositeContainer.add(buttonPanel, BorderLayout.SOUTH);
        mainComposite.add(filterCompositeContainer, BorderLayout.PAGE_START);
        mainComposite.add(resultComposite, BorderLayout.PAGE_END);
    }


    public JPanel getMainComposite() {
        return mainComposite;
    }
}
