package taridev.socle.presentation;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractDialog extends JFrame {

    protected JPanel mainComposite = new JPanel();
    private HeaderPanel headerPanel = new HeaderPanel();
    private JPanel optionPanel = new JPanel();


    public AbstractDialog() {
        this("");
    }

    public AbstractDialog(String title) {
        super(title);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        getContentPane().setLayout(new BorderLayout());
        setLocation((int) ((dimension.getWidth() - getWidth()) / 2), (int) ((dimension.getHeight() - getHeight()) / 2));
        getContentPane().add(headerPanel, BorderLayout.PAGE_START);
        getContentPane().add(mainComposite, BorderLayout.CENTER);
        getContentPane().add(optionPanel, BorderLayout.PAGE_END);
    }

    protected abstract void innerCreateContent();
}
