package taridev.socle.persistence;

import java.io.IOException;
import java.util.Comparator;

public abstract class AbstractXmlDaoWithAutoIncrement<E extends AbstractEntity<Integer>> extends AbstractXmlDao<E, Integer> {

    public AbstractXmlDaoWithAutoIncrement(final String filename) throws IOException {
        super(filename);
    }

    private Integer findLastInsertId() {
        E entityWithLastInsertId = collection.isEmpty() ? null : collection.stream().max(Comparator.comparing(E::getPK)).orElse(null);
        return entityWithLastInsertId == null ? 0 : entityWithLastInsertId.getPK();
    }

    @Override
    public void create(final E entity) {
        if (entity.getPK() == null) {
            entity.setPK(findLastInsertId() + 1);
        }
        this.collection.add(entity);
        try {
            xmlEncode();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
