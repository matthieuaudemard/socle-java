package taridev.socle.persistence;

public class DaoException extends Exception {

    public DaoException(String message) {
        super(message);
    }
}
