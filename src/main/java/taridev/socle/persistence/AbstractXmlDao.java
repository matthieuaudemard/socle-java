package taridev.socle.persistence;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractXmlDao<E extends AbstractEntity<K>, K extends Serializable & Comparable> implements Dao<E, K> {


    Set<E> collection;
    private String serializedFileName;

    AbstractXmlDao(final String serializedFileName) throws IOException {
        this.serializedFileName = serializedFileName;
        xmlDecode();

        if (this.collection == null) {
            this.collection = new HashSet<>();
        }

        this.serializedFileName = serializedFileName;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E find(K key) {
        return collection
                .stream()
                .filter(e -> e.getPK().compareTo(key) == 0).findFirst()
                .orElse(null);
    }

    @Override
    public Set<E> findAll() {
        return collection;
    }

    @Override
    public Boolean update(E entity) throws DaoException {
        E item = collection
                .stream()
                .filter(e -> e.equals(entity))
                .findFirst()
                .orElse(null);

        if (item != null) {
            collection.remove(item);
            collection.add(entity);
            try {
                xmlEncode();
            } catch (IOException e) {
                throw new DaoException("unable to xmldecode.");
            }

            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    @Override
    public Boolean delete(E entity) throws DaoException {
        Boolean result =  collection.remove(entity);

        if (result) {
            try {
                xmlEncode();
            } catch (IOException e) {
                throw new DaoException("unable to xmlencode");
            }
        }

        return result;
    }

    void xmlEncode() throws IOException {
        try(XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(this.serializedFileName)))) {
            encoder.writeObject(this.collection);
            encoder.flush();
        }
    }

    @SuppressWarnings("unchecked")
    private void xmlDecode() throws IOException {
        try(XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(this.serializedFileName)))) {
            collection = (Set<E>) decoder.readObject();
        }
        catch (FileNotFoundException e) {
            xmlEncode();
        }
    }
}
